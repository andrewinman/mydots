let mapleader = " "

let g:limelight_conceal_ctermfg = 240
let g:limelight_conceal_guifg = '#777777'

set wrap
set noswapfile incsearch
set shiftwidth=4 autoindent smartindent tabstop=4 softtabstop=4 expandtab
set scrolloff=999
set nofoldenable

set number relativenumber

"set viminfo='10,\"100,:20,%,n~/.viminfo
set viminfo='100,n$HOME/.config/.viminfo
set termguicolors
set linebreak
set spell spelllang=en_us
let g:airline#extensions#wordcount#enabled = 1
let g:airline#extensions#wordcount#filetypes = '\vnotes|help|markdown|rst|org|text|asciidoc|tex|mail|vimwiki'
let g:catppuccin_flavour = "mocha"
"Below is what I had cofigured prior to making the above line insert
"let g:airline_theme='gruvbox'

"Theme specific
"let g:tokyonight_style = 'night' " available: night, storm
"let g:tokyonight_enable_italic = 1

:hi CursorLine   cterm=bold ctermbg=white ctermfg=black 
:set cursorline

"Goyo Settings
function! s:goyo_enter()
  set noshowmode
  set noshowcmd
  :hi CursorLine   cterm=bold ctermbg=white ctermfg=black 
  :set cursorline
  Limelight
endfunction

function! s:goyo_leave()
  set showmode
  set showcmd
  :hi CursorLine   cterm=bold ctermbg=white ctermfg=black 
  :set cursorline
  Limelight!
endfunction

autocmd! User GoyoEnter nested call <SID>goyo_enter()
autocmd! User GoyoLeave nested call <SID>goyo_leave()

syntax on


