let data_dir = has('nvim') ? stdpath('data') . '/site' : '~/.vim'
if empty(glob(data_dir . '/autoload/plug.vim'))
  silent execute '!curl -fLo '.data_dir.'/autoload/plug.vim --create-dirs  https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim'
  autocmd VimEnter * PlugInstall --sync | source $MYVIMRC
endif
call plug#begin('~/.config/nvim/autoload/plugged')

Plug 'tpope/vim-surround'
Plug 'jiangmiao/auto-pairs'
Plug 'preservim/nerdtree'
Plug 'farmergreg/vim-lastplace'
"Plug 'mhinz/vim-startify'
"Plug 'joshdick/onedark.vim'
"Plug 'ghifarit53/tokyonight-vim'
"Plug 'sonph/onehalf', {'rtp': 'vim/'}
"Plug 'dracula/vim', { 'as': 'dracula' } 
"Plug 'sainnhe/sonokai'
"Plug 'sickill/vim-monokai'
"Plug 'bluz71/vim-moonfly-colors'
Plug 'morhetz/gruvbox'
Plug 'catppuccin/nvim', {'as': 'catppuccin'}
"Plug 'andreasvc/vim-256noir'
"Plug 'preservim/vim-pencil'
"Plug 'dpelle/vim-LanguageTool'
Plug 'vim-airline/vim-airline'
Plug 'vim-airline/vim-airline-themes'
Plug 'tonadev/vim-airline-256noir'
"Plug 'vimwiki/vimwiki'
Plug 'junegunn/limelight.vim'
Plug 'junegunn/goyo.vim'
Plug 'ap/vim-css-color'
Plug 'plasticboy/vim-markdown'
call plug#end()
