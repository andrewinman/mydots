#!/bin/sh

# compositor
killall picom
while pgrep -u $UID -x picom >/dev/null; do sleep 1; done
picom --config ~/.config/picom/picom.conf --experimental-backends &

~/.config/polybar/launch.sh &

#bg
nitrogen --restore &
#~/.fehbg &
clipmenud &
dunst &
#autotiling &

#setxkbmap -layout colemak &

#[ ! -s ~/.config/mpd/pid ] && mpd &
#/usr/lib/polkit-gnome/polkit-gnome-authentication-agent-1 &
#/usr/lib/polkit-kde-authentication-agent-1 &

#sxhkd
sxhkd -c $HOME/.config/i3/sxhkd/sxhkdrc &
